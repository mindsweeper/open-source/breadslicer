ARG DOCKER_BUILDKIT=1

FROM python:3.10-alpine as python-base
ENV VIRTUAL_ENV=.venv/
ENV POETRY_VIRTUALENVS_PATH=${VIRTUAL_ENV}
ENV WORK_DIR=/workspaces

WORKDIR ${WORK_DIR}
RUN apk update &&\
    apk add make git
RUN rm /var/cache/apk/*

FROM python-base as python-poetry

# Install poetry but clear all .cache in the same layer.
# If this is not done in the same layer it will still persist.
RUN pip install --upgrade pip &&\ 
    pip install --no-compile --no-cache-dir poetry &&\
    rm -rf /root/.cache/ &&\
    find /usr/local/lib -path '*/__pycache__*' -delete

FROM python-poetry as breadslicer-base
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV VIRTUAL_ENV=.venv/
COPY poetry.lock pyproject.toml Makefile README.md .print_help.py ${WORK_DIR}/

FROM breadslicer-base as breadslicer-dev
RUN make install-all
