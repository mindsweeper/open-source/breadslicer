#!/bin/sh

# scripts/version.sh
BASEDIR=$(dirname "$0")
latest_version=$($BASEDIR/version_sanitized.sh  $(git describe --tags --abbrev=1))
distance=$(git rev-list --count ${latest_version}..HEAD)
echo $latest_version-$distance

if [ "$distance" -eq 0 ]; then
    echo "File $file_name exists."
else
    echo "File $file_name does not exist."
fi