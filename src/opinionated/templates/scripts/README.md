# SCRIPTS

The purpose of this directory is to hold various utility scripts.  Primarily
these scripts are run through the `Makefile` to render the makefile more readable.
Secondary these scripts are meant to be called in an minimal environment where
there is no `make` installed. Such as storage conservative CI setup.