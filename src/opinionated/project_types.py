from enum import Enum


class ProjectTypes(Enum):
    app = "app"
    lib = "lib"
    django = "django"
    flask = "flask"
