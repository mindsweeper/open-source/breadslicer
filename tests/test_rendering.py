from pathlib import Path

from .utility import *  # noqa


def test_app(app_session: Path):
    assert not Path(
        app_session / "django"
    ).exists(), "Django folder was expected not to exists"


def test_app_pyproject(app_session: Path):
    assert Path(app_session / "pyproject.toml").exists()


def test_app_poetry(app_session: Path):
    assert Path(app_session / "poetry.lock").exists()


def test_app_make(app_session: Path):
    assert Path(app_session / "Makefile").exists()


def test_lib_files(lib_session: Path):
    assert Path(lib_session / "pyproject.toml").exists()
    assert not Path(
        lib_session / "django"
    ).exists(), "Django folder was expected not to exists"


def test_django(django_session: Path):
    assert Path(django_session / "pyproject.toml").exists()
    assert not Path(
        django_session / "django"
    ).exists(), "Django folder was expected not to exists"


def test_flask(flask_session: Path):
    assert Path(flask_session / "pyproject.toml").exists()
    assert not Path(
        flask_session / "django"
    ).exists(), "Django folder was expected not to exists"


def test_flask_make(flask_session: Path):
    assert Path(flask_session / "poetry.lock").exists()
