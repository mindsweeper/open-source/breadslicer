from argparse import Namespace
from pathlib import Path
from typing import Any, Dict

import pytest

from breadslicer.application.app import Application
from breadslicer.utility import fs


def create_project(answers: Dict[str, Any], tmp_path: Path) -> None:
    app = Application(url="opinionated", args=Namespace(output=tmp_path))
    app.bread.pre_render(answers=answers, directory=app.directory(answers))

    app.render(answers=answers)

    fs.cwd(app.directory(answers=answers))
    app.bread.post_render(answers=answers, directory=app.directory(answers))


@pytest.fixture(scope="session")
def app_session(tmp_path_factory: pytest.TempPathFactory) -> Path:
    name = "test_app"
    answers = {
        "project_name": name,
        "project_slug": name,
        "project_author_name": "test test",
        "project_author_email": "test test",
        "project_short_description": "test test test",
        "python_version": "3.10.12",
        "project_type": "app",
        "project_layout": "flat",
        "use_commit_hooks": False,
        "ci_system": "gitlab",
        "docker_base": "debian",
    }
    tmp_path = tmp_path_factory.mktemp(name)
    create_project(answers=answers, tmp_path=tmp_path)
    return tmp_path / name


@pytest.fixture(scope="session")
def lib_session(tmp_path_factory: pytest.TempPathFactory) -> Path:
    name = "test_lib"
    answers = {
        "project_name": name,
        "project_slug": name,
        "project_author_name": "test test",
        "project_author_email": "test test",
        "project_short_description": "test test test",
        "python_version": "3.10.12",
        "project_type": "lib",
        "project_layout": "src",
        "use_commit_hooks": True,
        "ci_system": "gitlab",
        "docker_base": "alpine",
    }

    tmp_path = tmp_path_factory.mktemp(name)
    create_project(answers=answers, tmp_path=tmp_path)
    return tmp_path / name


@pytest.fixture(scope="session")
def django_session(tmp_path_factory: pytest.TempPathFactory) -> Path:
    name = "test_django"
    answers = {
        "project_name": name,
        "project_slug": name,
        "project_author_name": "test test",
        "project_author_email": "test test",
        "project_short_description": "test test test",
        "python_version": "3.10.12",
        "project_type": "django",
        "project_layout": None,
        "use_commit_hooks": False,
        "ci_system": "gitlab",
        "docker_base": "debian-slim",
    }
    tmp_path = tmp_path_factory.mktemp(name)
    create_project(answers=answers, tmp_path=tmp_path)

    return tmp_path / name


@pytest.fixture(scope="session")
def flask_session(tmp_path_factory: pytest.TempPathFactory) -> Path:
    name = "test_flask"
    answers = {
        "project_name": name,
        "project_slug": name,
        "project_author_name": "test test",
        "project_author_email": "test test",
        "project_short_description": "test test test",
        "python_version": "3.10.12",
        "project_type": "flask",
        "project_layout": None,
        "use_commit_hooks": False,
        "ci_system": "gitlab",
        "docker_base": "debian",
    }
    tmp_path = tmp_path_factory.mktemp(name)
    create_project(answers=answers, tmp_path=tmp_path)

    return tmp_path / name
