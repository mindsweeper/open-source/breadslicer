#!/bin/sh

# scripts/version.sh
BASEDIR=$(dirname "$0")

latest_tag=$(git describe --tags --abbrev=0)
distance_tags=$(git rev-list --count ${latest_tag}..HEAD)

if [ "$distance_tags" -eq 0 ]; then
    echo $latest_tag
fi
latest_version=$($BASEDIR/version_sanitized.sh  $(git describe --tags --abbrev=1))
distance=$(git rev-list --count ${latest_version}..HEAD)
if [ "$distance" -eq 0 ]; then
    echo $latest_version
else
    echo $latest_version-$distance
fi